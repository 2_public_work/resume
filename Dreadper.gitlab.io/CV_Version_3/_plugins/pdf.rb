require 'pdfkit'
require 'pathname'

Jekyll::Hooks.register :site, :post_write do |site|
  # this is not the correct way to do this, but i really don't care
  # i will fix it later
  PDFKit.configure do |config|
    config.root_url = site.config["url"]
    config.verbose = true
  end
  kit = PDFKit.new(site.pages[0].to_s, :page_size => 'Letter', protocol: 'https', print_media_type: true, enable_local_file_access: true)
  kit.stylesheets << site.config["stylesheet"]
  file = kit.to_file("#{site.config['destination']}/#{site.config['downloads_dir']}/#{site.config['file_name']}") 
end