# CV pdf generation

forked from : https://gitlab.com/nyanbinary/jekyll-resume-template-pdf

## Usage :

- install dependancies :
```
bundle config set --local path 'vendor/bundle' && \
bundle install
```

- And then test it locally with jekyll's server :
```
bundle exec jekyll serve --livereload
```

- Or just build it :
```
bundle exec jekyll build 
```

## Configuration Variables :
In the `_config.yml` the following custom variables need to be set:
```yaml
. . .
download_dir: downloads # default is downloads, this is for where the pdf will be located
file_name: resume.pdf # name of the pdf
stylesheet: ./assets/main.css # name of the stylesheet for the pdf  
. . .
```
For production, you will need to set the `baseurl` variable, and the `url` variable. set the `url` to your site's domain, ie: `https://example.com`


## Sources : 

- jekyll and github page tutorial : https://www.aleksandrhovhannisyan.com/blog/getting-started-with-jekyll-and-github-pages/#4-running-jekyll-locally