# jekyl portofolio with rushes

## Usage 

- to install project and dependencies :
```
cd al-folio \ &
bundle config set --local path 'vendor/bundle' \ &
bundle install
```

- to start locally the project (dev web server on localhost:4000) : 
```
bundle exec jekyll serve
```

## Source : 

- markdown emoji list : https://github.com/markdown-templates/markdown-emojis